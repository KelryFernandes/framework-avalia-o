package com.example.projeto22.framework.Repositorio.Response;


import com.example.projeto22.framework.Classes.Retorno;

public interface RetornoListener {

  void onSuccess(Retorno retorno);
  void onFailure();

}
