package com.example.projeto22.framework.Repositorio;


import com.example.projeto22.framework.Repositorio.Response.ListarAlbumListener;
import com.example.projeto22.framework.Repositorio.Response.ListarTodosListener;
import com.example.projeto22.framework.Repositorio.Response.ListarCommentsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsUserIdListener;

public class BlogRepository {

  private RemoteRepository remoteRepository;


  public BlogRepository() {

    this.remoteRepository = new RemoteRepository();
  }

  public void listarPost(ListarPostsListener listener) {
    remoteRepository.listarPost(listener);
  }

  public void ListarComentarios(int postId,ListarCommentsListener listener) {
    remoteRepository.ListarComentarios(postId,listener);
  }

  public void listarTodosComentarios(ListarCommentsListener listener) {

    remoteRepository.ListarTodosComentarios(listener);
  }

  public void listarTodosPostsUserId(int UserId,ListarPostsUserIdListener listener) {
    remoteRepository.listarTodosPostsUserId(UserId,listener);
  }

  public void listarAlbum( ListarAlbumListener listener) {
    remoteRepository.listarAlbum(listener);
  }

  public void listarTodos(ListarTodosListener listener) {
    remoteRepository.listarTodos(listener);
  }



}
