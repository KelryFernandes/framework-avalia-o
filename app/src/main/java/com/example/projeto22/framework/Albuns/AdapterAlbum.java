package com.example.projeto22.framework.Albuns;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;

import java.util.ArrayList;


public class AdapterAlbum extends RecyclerView.Adapter<AdapterAlbum.ItemViewHolder> {

    private ArrayList<Album> Itens;
    private com.example.projeto22.framework.Interface.OnCheckChangeListener OnCheckChangeListener;
    private Context context;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListenerHack;

    public AdapterAlbum(ArrayList<Album> itens, Context cont) {
        this.Itens = itens;
        this.context = cont;
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListener r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    @Override
    public int getItemCount() {
        return Itens.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder ItemViewHolder, final int i) {

        final Album item = Itens.get(i);

        ItemViewHolder.Id.setText("Id: "+String.valueOf(item.getId()));
        ItemViewHolder.UserId.setText("UserId: "+String.valueOf(item.getUserId()));
        ItemViewHolder.title.setText("Title: "+item.getTitle());

    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemViewHolder itemViewHolderObject;
        View itemView;
        int res = 0;
        res = R.layout.adapter_album;

        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(res, viewGroup, false);
        itemViewHolderObject = new ItemViewHolder(itemView);
        return itemViewHolderObject;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView UserId;
        protected TextView Id;
        protected TextView title;


        public ItemViewHolder(View v) {
            super(v);

            Id = (TextView) v.findViewById(R.id.Id_Album);
            UserId = (TextView) v.findViewById(R.id.UserId_Album);
            title = (TextView) v.findViewById(R.id.Title_Album);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRecyclerViewOnClickListenerHack != null) {
                        mRecyclerViewOnClickListenerHack.onClickListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }
}
