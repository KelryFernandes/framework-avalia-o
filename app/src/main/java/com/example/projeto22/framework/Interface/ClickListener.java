package com.example.projeto22.framework.Interface;

public interface ClickListener {

    void onPositionClicked(int position);
    void onLongClicked(int position);
}
