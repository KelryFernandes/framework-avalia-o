package com.example.projeto22.framework.Repositorio;


import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Classes.Todos;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BlogApi {

 /* @POST(ApiConstants.LISTAR_TIPO_DE_CONSULTA_SML)
  MyCall<ArrayList<GetDocumentos>> login(@Body User user);*/


  @GET(ApiConstants.LISTAR_POST)
  Call<List<Posts>> listarPost();

  @GET(ApiConstants.LISTAR_COMENTARIOS)
  Call<List<Comments>> listarComentarios(@Query("postId") int postId);

  @GET(ApiConstants.LISTAR_COMENTARIOS)
  Call<List<Comments>> listarTodosComentarios();

  @GET(ApiConstants.LISTAR_POST)
  Call<List<Posts>> listarPostPorUserId(@Query("userId") int userId);

  @GET(ApiConstants.LISTAR_ALBUNS)
  Call<List<Album>> listarAlbum();

  @GET(ApiConstants.LISTAR_TODOS)
  Call<List<Todos>> listarTodos();

}
