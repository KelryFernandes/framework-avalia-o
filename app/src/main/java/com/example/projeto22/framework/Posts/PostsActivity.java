package com.example.projeto22.framework.Posts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Dados.MetodosBanco;
import com.example.projeto22.framework.Albuns.AlbumActivity;
import com.example.projeto22.framework.Comentarios.ComentariosAcvity;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;
import com.example.projeto22.framework.Todos.TodosActivity;
import com.example.projeto22.framework.Util.Util;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class PostsActivity extends AppCompatActivity implements MainContract.View, ActivityCompat.OnRequestPermissionsResultCallback{
    private RecyclerView my_recycler_view ;
    AdapterPosts adapterPosts;
    MetodosBanco metodosBanco;
    PostPresenter presenter = new PostPresenter(this);
    Button  bt_botao ;
    Button btn_pesquisa ,btn_todos;
    EditText UserId;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        Inicializar();

        if(Util.isOnline(this)){
            showProgressBar();
            presenter.listarPosts();}

        else {
            CarregarPosts(metodosBanco.Buscar_TodasPosts());
        }

        btn_todos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Util.isOnline(PostsActivity.this)){
                    showProgressBar();
                    presenter.listarPosts();
                }else
                {
                    CarregarPosts(metodosBanco.Buscar_TodasPosts());
                }

            }
        });

        btn_pesquisa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Util.isOnline(PostsActivity.this) )
                {
                    if(!UserId.getText().toString().equals("")){
                        presenter.listarPostsPorUserId(Integer.parseInt(UserId.getText().toString()));
                        showProgressBar();
                        UserId.setText("");
                    }
                    else
                        Util.dialog("É necessário informar o UserId para pesquisa.",PostsActivity.this);
                }else
                {
                    if(!UserId.getText().toString().equals("")){

                        List<Posts> postsBD = metodosBanco.BuscarItemPosts(Integer.parseInt(UserId.getText().toString()));
                        CarregarPosts(postsBD);
                        UserId.setText("");
                    }
                    else
                        Util.dialog("É necessário informar o UserId para pesquisa.",PostsActivity.this);
                }

            }
        });

    }

    private void Inicializar() {

        bt_botao = (Button) findViewById(R.id.btn_comentarios);
        btn_todos = (Button) findViewById(R.id.btn_todos);
        btn_pesquisa = (Button) findViewById(R.id.btn_pesquisaUserId);
        my_recycler_view = (RecyclerView)findViewById(R.id.recyclerView);
        my_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        my_recycler_view.setLayoutManager(layout);
        metodosBanco = new MetodosBanco(this);
        UserId = (EditText)findViewById(R.id.txt_pesquisaUserId);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_posts:
                        Intent it_posts = new Intent(PostsActivity.this, PostsActivity.class);
                        startActivity(it_posts);
                        break;
                    case R.id.action_album:
                        Intent it_album = new Intent(PostsActivity.this, AlbumActivity.class);
                        startActivity(it_album);
                        break;
                    case R.id.action_todos:
                        Intent it_todos = new Intent(PostsActivity.this, TodosActivity.class);
                        startActivity(it_todos);
                        break;
                }
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        presenter.dropView();
        super.onPause();
    }

    private void CarregarPosts(List<Posts> posts) {

        final List<Posts> postsList = posts;
        adapterPosts = new AdapterPosts((ArrayList<Posts>) posts, PostsActivity.this);

        adapterPosts.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {

                Intent it = new Intent(PostsActivity.this, ComentariosAcvity.class);
                Posts nf = postsList.get(position);
                Bundle bundle = new Bundle();
                bundle.putInt("postId", nf.getId());
                it.putExtras(bundle);
                startActivity(it);
            }
        });
        my_recycler_view.setAdapter(adapterPosts);

    }

    @Override
    public void didListarPotst(List<Posts> list) {
        hideProgressBar();
        metodosBanco.DeletarTodosPosts();
        metodosBanco.InserirPost(list);
        CarregarPosts(list);
    }

    @Override
    public void didBuscarTodosComentarios(List<Comments> list) {
        hideProgressBar();
        metodosBanco.DeletarTodosComentarios();
        metodosBanco.InserirComentarios(list);
    }

    @Override
    public void didListarPotstPorUserId(List<Posts> list) {
        hideProgressBar();
        CarregarPosts(list);
    }

    public void showProgressBar() {
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(INVISIBLE);
    }


}
