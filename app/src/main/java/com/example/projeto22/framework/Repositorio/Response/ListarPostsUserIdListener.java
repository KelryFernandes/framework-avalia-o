package com.example.projeto22.framework.Repositorio.Response;


import com.example.projeto22.framework.Classes.Posts;

import java.util.List;


public interface ListarPostsUserIdListener {
  void onSuccess(List<Posts> posts);
  void onFailure();

}
