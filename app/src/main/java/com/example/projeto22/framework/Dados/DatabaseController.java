package com.example.projeto22.framework.Dados;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseController extends SQLiteOpenHelper {

    private static String TAG = "Consulta_bd";
    private static final String NOME_BD = "BancoLocal.sqlite";
    private static final int VERSAO = 1;


    public DatabaseController(Context context) {
        super(context, NOME_BD, null, VERSAO);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table if not exists PostsBD " +
                "( _id integer primary key autoincrement, " +
                " userId integer," +
                " id integer," +
                " title text," +
                " body text);");

        sqLiteDatabase.execSQL("create table if not exists ComentariosBD " +
                "( _id integer primary key autoincrement, " +
                " postId integer," +
                " id integer," +
                " name text," +
                " email text," +
                " body text);");

        sqLiteDatabase.execSQL("create table if not exists AlbumBD " +
                "( _id integer primary key autoincrement, " +
                " userId integer," +
                " id integer," +
                " title text);");


        sqLiteDatabase.execSQL("create table if not exists TodosBD " +
                "( _id integer primary key autoincrement, " +
                " userId integer," +
                " id integer," +
                " title text," +
                " completed bit);");

    }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
            sqLiteDatabase.execSQL("drop table PostsBD;");
            sqLiteDatabase.execSQL("drop table ComentariosBD;");
            sqLiteDatabase.execSQL("drop table TodosBD;");
            sqLiteDatabase.execSQL("drop table AlbumBD;");
            onCreate(sqLiteDatabase);
        }

}
