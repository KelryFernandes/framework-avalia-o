package com.example.projeto22.framework.Repositorio.Response;


import com.example.projeto22.framework.Classes.Todos;

import java.util.List;


public interface ListarTodosListener {
  void onSuccess(List<Todos> todos);
  void onFailure();

}
