package com.example.projeto22.framework.Repositorio.Response;

import java.io.IOException;

import retrofit2.Response;

public interface MyCall<T> {

    void cancel();
    void enqueue(MyCallBack<T> callback);
    MyCall<T> clone();
    Response<T> execute() throws IOException;
}
