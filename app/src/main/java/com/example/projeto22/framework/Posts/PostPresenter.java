package com.example.projeto22.framework.Posts;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;


import androidx.appcompat.widget.Toolbar;

import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Repositorio.RemoteRepository;
import com.example.projeto22.framework.Repositorio.Response.ListarCommentsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsUserIdListener;
import com.example.projeto22.framework.Util.Util;

import java.util.List;


public class PostPresenter implements MainContract.Presenter {

    private final RemoteRepository repository;
    private MainContract.View view;
    private Activity context;


    public PostPresenter(Activity context) {
        this.repository = new RemoteRepository();
        this.context = context;
    }

    void takeView(MainContract.View view) {
        this.view = view;
    }

    void dropView() {
        view = null;
    }

    public void dialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public void dialogShowErroAndFinish(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                context.finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void listarPosts() {

        repository.listarPost(new ListarPostsListener() {
            @Override
            public void onSuccess(List<Posts> list) {
                InserirComentarios();
                view.didListarPotst(list);

            }

            @Override
            public void onFailure() {
                if (view != null) {
                    Util.dialog("Ocorreu alguma instabilidade no sistema, verificar a conexão ou acessar desconectado.",context);
                }
            }
        });
    }

    @Override
    public void InserirComentarios() {

        repository.ListarTodosComentarios(new ListarCommentsListener() {
            @Override
            public void onSuccess(List<Comments> list) {
                view.didBuscarTodosComentarios(list);
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    //view.didListarTipoDeConsultaFail();
                }
            }
        });

    }


    @Override
    public void listarPostsPorUserId(int userId) {

        repository.listarTodosPostsUserId(userId, new ListarPostsUserIdListener() {
            @Override
            public void onSuccess(List<Posts> list) {
                view.didListarPotstPorUserId(list);

            }

            @Override
            public void onFailure() {
                if (view != null) {
                    //view.didListarTipoDeConsultaFail();
                }
            }
        });
    }
}
