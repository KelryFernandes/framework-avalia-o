package com.example.projeto22.framework.Comentarios;


import com.example.projeto22.framework.Classes.Comments;

import java.util.List;

public interface ComentariosContract {

	interface View {
		void didListarComentarios(List<Comments> list);
	}

	interface Presenter {
		void listarComentarios(int postId);
	}
}
