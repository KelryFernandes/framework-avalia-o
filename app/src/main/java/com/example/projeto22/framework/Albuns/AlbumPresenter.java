package com.example.projeto22.framework.Albuns;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Repositorio.Response.ListarAlbumListener;
import com.example.projeto22.framework.Repositorio.RemoteRepository;
import com.example.projeto22.framework.Util.Util;

import java.util.List;


public class AlbumPresenter implements AlbumContract.Presenter {

    private final RemoteRepository repository;
    private AlbumContract.View view;
    private Activity context;

    public AlbumPresenter(Activity context) {
        this.repository = new RemoteRepository();
        this.context = context;
    }

    void takeView(AlbumContract.View view) {
        this.view = view;
    }

    void dropView() {
        view = null;
    }

    public void dialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void dialogShowErroAndFinish(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                context.finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }



    @Override
    public void listarAlbum() {

        repository.listarAlbum(new ListarAlbumListener() {
            @Override
            public void onSuccess(List<Album> list) {
                view.didListarAlbum(list);
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    Util.dialog("Ocorreu alguma instabilidade no sistema, verificar a conexão ou acessar desconectado.",context);
                }
            }
        });

    }
}
