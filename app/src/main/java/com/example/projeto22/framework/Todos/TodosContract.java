package com.example.projeto22.framework.Todos;


import com.example.projeto22.framework.Classes.Todos;

import java.util.List;

public interface TodosContract {

	interface View {
		void didListarTodos(List<Todos> list);
	}

	interface Presenter {
		void listarTodos();
	}
}
