package com.example.projeto22.framework.Repositorio.Response;


import com.example.projeto22.framework.Classes.Comments;

import java.util.List;


public interface ListarCommentsListener {
  void onSuccess(List<Comments> comentarios);
  void onFailure();

}
