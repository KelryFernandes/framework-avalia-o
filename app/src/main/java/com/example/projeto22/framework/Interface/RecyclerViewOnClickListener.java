package com.example.projeto22.framework.Interface;

import android.view.View;

/**
 * Created by viniciusthiengo on 4/5/15.
 */
public interface RecyclerViewOnClickListener {
    public void onClickListener(View view, int position);
}
