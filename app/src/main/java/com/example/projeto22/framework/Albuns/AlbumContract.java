package com.example.projeto22.framework.Albuns;


import com.example.projeto22.framework.Classes.Album;

import java.util.List;

public interface AlbumContract {

	interface View {
		void didListarAlbum(List<Album> list);
	}

	interface Presenter {
		void listarAlbum();
	}
}
