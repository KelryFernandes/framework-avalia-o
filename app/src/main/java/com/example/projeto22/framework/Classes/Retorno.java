package com.example.projeto22.framework.Classes;

public class Retorno {

        private int StatusCode;
        private String Status;
        private String Mensagem;

        public int getStatusCode() {
            return StatusCode;
        }

        public void setStatusCode(int statusCode) {
            StatusCode = statusCode;
        }

        public String getStatus() {
            return Status;
        }

        public void setStatus(String status) {
            Status = status;
        }

        public String getMensagem() {
            return Mensagem;
        }

        public void setMensagem(String mensagem) {
            Mensagem = mensagem;
        }
}
