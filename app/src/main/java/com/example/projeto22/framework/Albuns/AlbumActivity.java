package com.example.projeto22.framework.Albuns;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Dados.MetodosBanco;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;
import com.example.projeto22.framework.Util.Util;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class AlbumActivity extends AppCompatActivity implements AlbumContract.View, ActivityCompat.OnRequestPermissionsResultCallback {

    AdapterAlbum adapterAlbum;
    MetodosBanco metodosBanco;
    AlbumPresenter presenter = new AlbumPresenter(this);
    private RecyclerView my_recycler_view ;
    private Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);
        Inicializar();

        if(Util.isOnline(this)){
            showProgressBar();
            presenter.listarAlbum();}

        else {
            CarregarAlbuns(metodosBanco.Buscar_TodasAlbum());
        }


    }
    private void Inicializar() {

        my_recycler_view = (RecyclerView)findViewById(R.id.recyclerViewAlbum);
        my_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        my_recycler_view.setLayoutManager(layout);
        metodosBanco = new MetodosBanco(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        presenter.dropView();
        super.onPause();
    }

    private void CarregarAlbuns(List<Album> albumList) {

        adapterAlbum = new AdapterAlbum((ArrayList<Album>)albumList , AlbumActivity.this);
        adapterAlbum.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {

                Intent it = new Intent(AlbumActivity.this, AlbumActivity.class);
                startActivity(it);

            }
        });
        my_recycler_view.setAdapter(adapterAlbum);
    }


    @Override
    public void didListarAlbum(List<Album> list) {
        metodosBanco.DeletarTodosAlbum();
        metodosBanco.InserirAlbum(list);
        hideProgressBar();
        CarregarAlbuns(list);
    }

    public void showProgressBar() {
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(INVISIBLE);
    }
}
