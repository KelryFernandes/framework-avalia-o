package com.example.projeto22.framework;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Dados.MetodosBanco;
import com.example.projeto22.framework.Posts.PostsActivity;
import com.example.projeto22.framework.Util.Util;
import com.example.projeto22.framework.R;

import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;


public class MainActivity extends AppCompatActivity {

    EditText Usuario,Senha;
    private Button btn;
    MetodosBanco metodosBanco;
    ProgressBar progressBar;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Inicializar();

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Usuario.getText().toString().equals("") || Senha.getText().toString().equals(""))
                {
                    Util.dialog("É necessário que todos campos sejam preenchidos", MainActivity.this);
                }else{

                    if(Util.isOnline(MainActivity.this))
                    {
                        Intent it = new Intent(MainActivity.this, PostsActivity.class);
                        startActivity(it);

                    }else
                    {
                       List<Posts> PostsOf= metodosBanco.Buscar_TodasPosts();
                       if(PostsOf.size()>0)
                           dialog("Você está sem acesso a internet no momento. Porém como não é seu primeiro acesso, poderá continuar.");
                       else
                           Util.dialog("Você está sem acesso a internet no momento. Como é seu primeiro acesso, é necessário que esteja conectado.",MainActivity.this);


                    }
                }
            }
        });

    }

    private void Inicializar() {

        btn = (Button) findViewById(R.id.btn_logar);
        Usuario = (EditText) findViewById(R.id.txt_usuario);
        Senha = (EditText) findViewById(R.id.txt_senha);
        metodosBanco = new MetodosBanco(this);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
    }

    public void dialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

                Intent it = new Intent(MainActivity.this, PostsActivity.class);
                startActivity(it);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void showProgressBar() {
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(INVISIBLE);
    }

}
