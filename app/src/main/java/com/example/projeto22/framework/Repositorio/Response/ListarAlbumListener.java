package com.example.projeto22.framework.Repositorio.Response;


import com.example.projeto22.framework.Classes.Album;

import java.util.List;


public interface ListarAlbumListener {
  void onSuccess(List<Album> albums);
  void onFailure();

}
