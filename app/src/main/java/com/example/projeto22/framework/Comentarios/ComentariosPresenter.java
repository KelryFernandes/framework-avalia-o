package com.example.projeto22.framework.Comentarios;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Repositorio.RemoteRepository;
import com.example.projeto22.framework.Repositorio.Response.ListarCommentsListener;
import com.example.projeto22.framework.Util.Util;

import java.util.List;


public class ComentariosPresenter implements ComentariosContract.Presenter {

    private final RemoteRepository repository;
    private ComentariosContract.View view;
    private Activity context;

    public ComentariosPresenter(Activity context) {
        this.repository = new RemoteRepository();
        this.context = context;
    }

    void takeView(ComentariosContract.View view) {
        this.view = view;
    }

    void dropView() {
        view = null;
    }

    public void dialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void dialogShowErroAndFinish(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                context.finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void listarComentarios(int postId) {
        repository.ListarComentarios(postId,new ListarCommentsListener() {
            @Override
            public void onSuccess(List<Comments> list) {
                     view.didListarComentarios(list);
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    Util.dialog("Ocorreu alguma instabilidade no sistema, verificar a conexão ou acessar desconectado.",context);
                }
            }
        });
    }
}
