package com.example.projeto22.framework.Posts;


import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;

import java.util.List;

public interface MainContract {

	interface View {
		void didListarPotst(List<Posts> list);
		void didListarPotstPorUserId(List<Posts> list);
		void didBuscarTodosComentarios(List<Comments> list);
	}

	interface Presenter {
		void listarPosts();
		void InserirComentarios();
		void listarPostsPorUserId(int UserId);

	}
}
