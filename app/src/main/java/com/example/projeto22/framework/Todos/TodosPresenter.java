package com.example.projeto22.framework.Todos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

import com.example.projeto22.framework.Classes.Todos;
import com.example.projeto22.framework.Repositorio.RemoteRepository;
import com.example.projeto22.framework.Repositorio.Response.ListarTodosListener;
import com.example.projeto22.framework.Util.Util;

import java.util.List;


public class TodosPresenter implements TodosContract.Presenter {

    private final RemoteRepository repository;
    private TodosContract.View view;
    private Activity context;

    public TodosPresenter(Activity context) {
        this.repository = new RemoteRepository();
        this.context = context;
    }

    void takeView(TodosContract.View view) {
        this.view = view;
    }

    void dropView() {
        view = null;
    }

    public void dialog(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void dialogShowErroAndFinish(String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Atenção!");
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                context.finish();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }



    @Override
    public void listarTodos() {

        repository.listarTodos(new ListarTodosListener() {
            @Override
            public void onSuccess(List<Todos> list) {
                view.didListarTodos(list);
            }

            @Override
            public void onFailure() {
                if (view != null) {
                    Util.dialog("Ocorreu alguma instabilidade no sistema, verificar a conexão ou acessar desconectado.",context);
                }
            }
        });

    }
}
