package com.example.projeto22.framework.Comentarios;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Dados.MetodosBanco;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;
import com.example.projeto22.framework.Util.Util;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class ComentariosAcvity extends AppCompatActivity implements ComentariosContract.View, ActivityCompat.OnRequestPermissionsResultCallback {

   private RecyclerView my_recycler_view ;
    AdapterComentarios adapterComentarios;
    MetodosBanco metodosBanco;
    ComentariosPresenter presenter = new ComentariosPresenter(this);
    private Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentarios);
        Inicializar();
        Bundle ObjetoRecebido = getIntent().getExtras();
        if (ObjetoRecebido != null) {
            int postId= ObjetoRecebido.getInt("postId");

            if(Util.isOnline(this)){
                showProgressBar();
                presenter.listarComentarios(postId);
            }else{
                CarregarComentarios(metodosBanco.BuscarItemCometario(postId));
            }

        } else {
           Util.dialog("Problemas de instabilidade no sistema, entrar em contato com o responsavel.",this);
        }
    }


    private void Inicializar() {

        my_recycler_view = (RecyclerView)findViewById(R.id.recyclerViewComentariosAcvity);
        my_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        my_recycler_view.setLayoutManager(layout);
        metodosBanco = new MetodosBanco(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        presenter.dropView();
        super.onPause();
    }

    private void CarregarComentarios(List<Comments> Comments) {

        adapterComentarios = new AdapterComentarios((ArrayList<Comments>)Comments , ComentariosAcvity.this);
        adapterComentarios.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {

                Intent it = new Intent(ComentariosAcvity.this, ComentariosAcvity.class);
                startActivity(it);

            }
        });
        my_recycler_view.setAdapter(adapterComentarios);
    }

    @Override
    public void didListarComentarios(List<Comments> list) {
        hideProgressBar();
        CarregarComentarios(list);
    }

    public void showProgressBar() {
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(INVISIBLE);
    }

}
