package com.example.projeto22.framework.Repositorio;



public interface ApiConstants {

    String LISTAR_POST = "posts";
    String LISTAR_COMENTARIOS = "comments";
    String LISTAR_ALBUNS = "albums";
    String LISTAR_TODOS = "todos";
}
