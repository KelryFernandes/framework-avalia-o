package com.example.projeto22.framework.Posts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;

import java.util.ArrayList;


public class AdapterPosts extends RecyclerView.Adapter<AdapterPosts.ItemViewHolder> {

    private ArrayList<Posts> Itens;
    private com.example.projeto22.framework.Interface.OnCheckChangeListener OnCheckChangeListener;
    private Context context;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListenerHack;

    public AdapterPosts(ArrayList<Posts> itens, Context cont) {
        this.Itens = itens;
        this.context = cont;
    }


    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListener r) {
        mRecyclerViewOnClickListenerHack = r;
    }


    @Override
    public int getItemCount() {
        return Itens.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder ItemViewHolder, final int i) {
        final Posts item = Itens.get(i);
        ItemViewHolder.Id.setText("Id:"+String.valueOf(item.getId()));
        ItemViewHolder.Title.setText("TItle:"+item.getTitle());
        ItemViewHolder.UserId.setText("UserId:"+String.valueOf(item.getUserId()));
        ItemViewHolder.Body.setText("Body:"+item.getBody());
    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemViewHolder itemViewHolderObject;
        View itemView;
        int res = 0;
        res = R.layout.adapterdetalhes;

        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(res, viewGroup, false);
        itemViewHolderObject = new ItemViewHolder(itemView);
        return itemViewHolderObject;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView Body, Title;
        protected TextView Id;
        protected TextView UserId;
        protected Button bt_botao ;

        public ItemViewHolder(View v) {
            super(v);

            Id = (TextView) v.findViewById(R.id.id);
            UserId = (TextView) v.findViewById(R.id.idUser);
            Body = (TextView) v.findViewById(R.id.Body);
            Title = (TextView) v.findViewById(R.id.Tittle);
            bt_botao = (Button) v.findViewById(R.id.btn_comentarios);


            bt_botao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRecyclerViewOnClickListenerHack != null) {
                        mRecyclerViewOnClickListenerHack.onClickListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }
}
