package com.example.projeto22.framework.Dados;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Classes.Todos;

import java.util.ArrayList;
import java.util.List;

public class MetodosBanco {

    SQLiteDatabase bd;
    private DatabaseController mDatabaseController;


    public MetodosBanco(Context context) {
        DatabaseController auxBD = new DatabaseController(context);

        bd = auxBD.getWritableDatabase();

    }

    public void InserirPost(List<Posts> posts) {

        ContentValues values = new ContentValues();
        for (Posts posts1 : posts) {

            values.put("id", posts1.getId());
            values.put("userId", posts1.getUserId());
            values.put("title", posts1.getTitle());
            values.put("body", posts1.getBody());

            bd.insert("PostsBD", null, values);
        }
    }

    public void InserirComentarios(List<Comments> comments) {

        ContentValues values = new ContentValues();

        for (Comments comments1 : comments) {

            values.put("id", comments1.getId());
            values.put("postId", comments1.getPostId());
            values.put("name", comments1.getName());
            values.put("email", comments1.getEmail());
            values.put("body", comments1.getBody());

            bd.insert("ComentariosBD", null, values);
        }
    }

    public void InserirAlbum(List<Album> albums) {

        ContentValues values = new ContentValues();
        for (Album albumls : albums) {

            values.put("userId", albumls.getUserId());
            values.put("id", albumls.getId());
            values.put("title", albumls.getTitle());

            bd.insert("AlbumBD", null, values);
        }
    }

    public void InserirTodos(List<Todos> todos) {

        ContentValues values = new ContentValues();
        for (Todos todosls : todos) {

            values.put("id", todosls.getId());
            values.put("userId", todosls.getUserId());
            values.put("title", todosls.getTitle());
            values.put("completed", todosls.isCompleted());

            bd.insert("TodosBD", null, values);
        }
    }

    public void DeletarTodosPosts() {
        bd.delete("PostsBD", null, null);
    }

    public void DeletarTodosComentarios() {
        bd.delete("ComentariosBD", null, null);
    }

    public void DeletarTodosAlbum() {
        bd.delete("AlbumBD", null, null);
    }

    public void DeletarTodos() {
        bd.delete("TodosBD", null, null);
    }

    public List<Posts> Buscar_TodasPosts() {
        List<Posts> list = new ArrayList<Posts>();
        String[] colunas = new String[]{"_id", "userId", "title", "body"};

        Cursor cursor = bd.query("PostsBD", colunas,
                null, null, null, null,
                null);
        try {
            bd.beginTransaction();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    Posts get_posts = new Posts();

                    get_posts.setId(cursor.getInt(0));
                    get_posts.setUserId(cursor.getInt(1));
                    get_posts.setTitle(cursor.getString(2));
                    get_posts.setBody(cursor.getString(3));
                    list.add(get_posts);

                } while (cursor.moveToNext());
                bd.setTransactionSuccessful();
            }
            return list;

        } catch (SQLException e) {
            throw e;
        } finally {
            bd.endTransaction();
        }
    }

    public List<Album> Buscar_TodasAlbum() {

        List<Album> list = new ArrayList<Album>();
        String[] colunas = new String[]{"_id", "userId", "id", "title"};

        Cursor cursor = bd.query("AlbumBD", colunas,
                null, null, null, null,
                null);
        try {
            bd.beginTransaction();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    Album getAlbum = new Album();

                    getAlbum.setUserId(cursor.getInt(1));
                    getAlbum.setId(cursor.getInt(2));
                    getAlbum.setTitle(cursor.getString(3));
                    list.add(getAlbum);

                } while (cursor.moveToNext());
                bd.setTransactionSuccessful();
            }
            return list;

        } catch (SQLException e) {
            throw e;
        } finally {
            bd.endTransaction();
        }
    }

    public List<Todos> Buscar_Todas() {
        List<Todos> list = new ArrayList<Todos>();
        String[] colunas = new String[]{"_id", "id", "userId", "title","completed"};

        Cursor cursor = bd.query("TodosBD", colunas,
                null, null, null, null,
                null);
        try {
            bd.beginTransaction();
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {

                    Todos getTodos = new Todos();

                    getTodos.setId(cursor.getInt(1));
                    getTodos.setUserId(cursor.getInt(2));
                    getTodos.setTitle(cursor.getString(3));
                    getTodos.setCompleted(cursor.getInt(4) != 0);
                    list.add(getTodos);

                } while (cursor.moveToNext());
                bd.setTransactionSuccessful();
            }
            return list;

        } catch (SQLException e) {
            throw e;
        } finally {
            bd.endTransaction();
        }
    }

    public List<Posts> BuscarItemPosts(int UserId) {

        ArrayList<Posts> list = new ArrayList<>();
        String[] colunas = new String[]{"_id","userId", "id", "title", "body"};


        Cursor cursor = bd.rawQuery("Select *from PostsBD" + " where userId=" + UserId, null);

        try {
            bd.beginTransaction();

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                do {
                    Posts _posts = new Posts();
                    _posts.setId(cursor.getInt(2));
                    _posts.setUserId(cursor.getInt(1));
                    _posts.setTitle(cursor.getString(3));
                    _posts.setBody(cursor.getString(4));

                    list.add(_posts);

                } while (cursor.moveToNext());
            }
            bd.setTransactionSuccessful();
            return list;

        } catch (SQLException e) {
            throw e;
        } finally {
            bd.endTransaction();
        }
    }

    public List<Comments> BuscarItemCometario(int PostId) {

         ArrayList<Comments> list = new ArrayList<>();

        String[] colunas = new String[]{"_id", "postId","id", "name", "email", "body"};
        Cursor cursor = bd.rawQuery("Select *from ComentariosBD" + " where postId=" + PostId, null);

        try {
            bd.beginTransaction();

            if (cursor.getCount() > 0) {

                cursor.moveToFirst();
                do {
                    Comments _Comments = new Comments();

                    _Comments.setId(cursor.getInt(2));
                    _Comments.setPostId(cursor.getInt(1));
                    _Comments.setName(cursor.getString(3));
                    _Comments.setEmail(cursor.getString(4));
                    _Comments.setBody(cursor.getString(5));

                    list.add(_Comments);

                } while (cursor.moveToNext());
            }
            bd.setTransactionSuccessful();
            return list;

        } catch (SQLException e) {
            throw e;
        } finally {
            bd.endTransaction();
        }
    }
}
