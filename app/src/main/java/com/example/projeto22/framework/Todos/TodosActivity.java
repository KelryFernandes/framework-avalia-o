package com.example.projeto22.framework.Todos;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projeto22.framework.Dados.MetodosBanco;
import com.example.projeto22.framework.Classes.Todos;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;
import com.example.projeto22.framework.Util.Util;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;

public class TodosActivity extends AppCompatActivity implements TodosContract.View, ActivityCompat.OnRequestPermissionsResultCallback {

    AdapterTodos adapterTodos;
    MetodosBanco metodosBanco;
    TodosPresenter presenter = new TodosPresenter(this);
    private RecyclerView my_recycler_view ;
    private Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todos);
        Inicializar();
        if(Util.isOnline(this)){
            showProgressBar();
            presenter.listarTodos();}

        else {
            CarregarTodos(metodosBanco.Buscar_Todas());
        }


    }
    private void Inicializar() {

        my_recycler_view = (RecyclerView)findViewById(R.id.recyclerViewTodos);
        my_recycler_view.setHasFixedSize(true);
        RecyclerView.LayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        my_recycler_view.setLayoutManager(layout);
        metodosBanco = new MetodosBanco(this);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.takeView(this);
    }

    @Override
    public void onPause() {
        presenter.dropView();
        super.onPause();
    }

    private void CarregarTodos(List<Todos> todos) {

        adapterTodos = new AdapterTodos((ArrayList<Todos>)todos , TodosActivity.this);
        adapterTodos.setRecyclerViewOnClickListenerHack(new RecyclerViewOnClickListener() {
            @Override
            public void onClickListener(View view, int position) {

                Intent it = new Intent(TodosActivity.this, TodosActivity.class);
                startActivity(it);

            }
        });
        my_recycler_view.setAdapter(adapterTodos);
    }


    @Override
    public void didListarTodos(List<Todos> list) {
        metodosBanco.DeletarTodos();
        metodosBanco.InserirTodos(list);
        hideProgressBar();
        CarregarTodos(list);

    }

    public void showProgressBar() {
        progressBar.setVisibility(VISIBLE);
    }

    public void hideProgressBar() {
        progressBar.setVisibility(INVISIBLE);
    }

}
