package com.example.projeto22.framework.Repositorio;


import com.example.projeto22.framework.Classes.Album;
import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Classes.Posts;
import com.example.projeto22.framework.Classes.Todos;
import com.example.projeto22.framework.Repositorio.Response.ListarAlbumListener;
import com.example.projeto22.framework.Repositorio.Response.ListarTodosListener;
import com.example.projeto22.framework.Util.Constants;
import com.example.projeto22.framework.Repositorio.Response.ListarCommentsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsListener;
import com.example.projeto22.framework.Repositorio.Response.ListarPostsUserIdListener;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RemoteRepository {

    private BlogApi Api;

    public RemoteRepository() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.URL_CONEXAO_)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api = retrofit.create(BlogApi.class);

    }

    public void listarPost(final ListarPostsListener listener) {

        Api.listarPost().enqueue(new Callback<List<Posts>>() {
            @Override
            public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {
                listener.onSuccess(response.body());

            }

            @Override
            public void onFailure(Call<List<Posts>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }

    public void ListarComentarios(int postId,final ListarCommentsListener listener) {
        Api.listarComentarios(postId).enqueue(new Callback<List<Comments>>() {
            @Override
            public void onResponse(Call<List<Comments>> call, Response<List<Comments>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Comments>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }

    public void ListarTodosComentarios(final ListarCommentsListener listener) {
        Api.listarTodosComentarios().enqueue(new Callback<List<Comments>>() {
            @Override
            public void onResponse(Call<List<Comments>> call, Response<List<Comments>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Comments>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }

    public void listarTodosPostsUserId(int UserId,final ListarPostsUserIdListener listener) {

        Api.listarPostPorUserId(UserId).enqueue(new Callback<List<Posts>>() {
            @Override
            public void onResponse(Call<List<Posts>> call, Response<List<Posts>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Posts>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }


    public void listarAlbum(final ListarAlbumListener listener) {

        Api.listarAlbum().enqueue(new Callback<List<Album>>() {
            @Override
            public void onResponse(Call<List<Album>> call, Response<List<Album>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Album>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }

    public void listarTodos(final ListarTodosListener listener) {

        Api.listarTodos().enqueue(new Callback<List<Todos>>() {
            @Override
            public void onResponse(Call<List<Todos>> call, Response<List<Todos>> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<List<Todos>> call, Throwable t) {
                listener.onFailure();
            }
        });
    }
}
