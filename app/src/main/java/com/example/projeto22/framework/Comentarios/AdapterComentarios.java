package com.example.projeto22.framework.Comentarios;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.projeto22.framework.Classes.Comments;
import com.example.projeto22.framework.Interface.RecyclerViewOnClickListener;
import com.example.projeto22.framework.R;

import java.util.ArrayList;


public class AdapterComentarios extends RecyclerView.Adapter<AdapterComentarios.ItemViewHolder> {

    private ArrayList<Comments> Itens;
    private com.example.projeto22.framework.Interface.OnCheckChangeListener OnCheckChangeListener;
    private Context context;
    private RecyclerViewOnClickListener mRecyclerViewOnClickListenerHack;

    public AdapterComentarios(ArrayList<Comments> itens, Context cont) {
        this.Itens = itens;
        this.context = cont;
    }

    public void setRecyclerViewOnClickListenerHack(RecyclerViewOnClickListener r) {
        mRecyclerViewOnClickListenerHack = r;
    }

    @Override
    public int getItemCount() {
        return Itens.size();
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder ItemViewHolder, final int i) {

        final Comments item = Itens.get(i);

        ItemViewHolder.Id.setText("Id:"+String.valueOf(item.getId()));
        ItemViewHolder.name.setText("Name:"+item.getName());
        ItemViewHolder.postId.setText("PostId:"+String.valueOf(item.getPostId()));
        ItemViewHolder.Body.setText("Body:"+item.getBody());
        ItemViewHolder.email.setText("Email:"+item.getEmail());
    }

    @Override
    public int getItemViewType(int position) {

        return 0;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        ItemViewHolder itemViewHolderObject;
        View itemView;
        int res = 0;
        res = R.layout.adaptercomentarios;

        itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(res, viewGroup, false);
        itemViewHolderObject = new ItemViewHolder(itemView);
        return itemViewHolderObject;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        protected TextView Body;
        protected TextView Id;
        protected TextView postId;
        protected TextView name;
        protected TextView email;


        public ItemViewHolder(View v) {
            super(v);

            Id = (TextView) v.findViewById(R.id.id_comentarios);
            postId = (TextView) v.findViewById(R.id.postId);
            Body = (TextView) v.findViewById(R.id.body);
            email = (TextView) v.findViewById(R.id.email);
            name = (TextView) v.findViewById(R.id.name);



            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mRecyclerViewOnClickListenerHack != null) {
                        mRecyclerViewOnClickListenerHack.onClickListener(v, getLayoutPosition());
                    }
                }
            });
        }
    }
}
